package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class ImageSerializerBase64Impl implements ImageSerializer {
    
	@Override
	public String serialize(File image){
		try {
			OutputStream os = new FileOutputStream (image);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

	@Override
	public byte[] deserialize(String encodedImage) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream( baos );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			oos.writeObject( encodedImage );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] byteArray = baos.toByteArray();
		for ( byte b : byteArray ) {
			System.out.print( (char) b );
		 }
		return byteArray;
	}
}
	
