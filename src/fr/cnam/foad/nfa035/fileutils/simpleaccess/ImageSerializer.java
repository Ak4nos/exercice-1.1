package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;

public interface ImageSerializer {

	String serialize(File image) throws IOException;

	byte[] deserialize(String encodedImage);


}
